class User < ActiveRecord::Base
	#remember_token is not in the database, therefore it's created as an attribute.
	attr_accessor :remember_token

	before_save { email.downcase! } #the word self is optional on the right hand side.
	validates :name, presence: true, length: { maximum: 50 }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
	validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
	
	#password - using bcrypt - password_digest added to database schema. 
	has_secure_password
	validates :password, length: { minimum: 6 }, allow_blank: true

	# Returns the hash digest of the given string.
	def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  #Returns a random token string. 
  def User.new_token
  	SecureRandom.urlsafe_base64
  end

  #Remembers a user in the databse for use in persistent sessions.
  def remember
  	self.remember_token = User.new_token
  	update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  #Returns true if the given token matches the digest.
  def authenticated?(remember_token)
    return false if remember_digest.nil? #returns false instead of rasing an error. 
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  #Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
end


