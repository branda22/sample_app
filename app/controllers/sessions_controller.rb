class SessionsController < ApplicationController
  def new
    #login html page.
  end

  def create #POST HTTP METHOD
  	user = User.find_by(email: params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
  		log_in(user)
      if params[:session][:remember_me] == '1' # '1' A STRING not a integer! 
        remember(user)
      else
        forget(user)
      end
  		redirect_back_or user #redirect back to last page or the the user profile.
  	else
  		flash.now[:danger] = 'Invalid email/password combination' #this is not quite right yet.
  		render 'new'
  	end
  end

  def destroy #DELETE HTTP METHOD
    log_out if logged_in? # if logged_in? prevents errors when multiple browsers are logged in.
    redirect_to root_url
  end
end
