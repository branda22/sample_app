module SessionsHelper
	#logs in the current user
	def log_in(user)
		session[:user_id] = user.id
	end

	def remember(user)
		user.remember
		cookies.permanent.signed[:user_id] = user.id
		cookies.permanent.signed[:remember_token] = user.remember_token
	end
	
	#return true if the given user is the current user
	def current_user?(user)
		user == current_user
	end
		
	#Returns the current user corresponding to the remember token cookie
	def current_user
		#if session[:user_id] exists set it to user_id variable
		if (user_id = session[:user_id])
			@current_user ||= User.find_by(id: user_id)
		# else if cookies.signed[:user_id] is valid set it to user_id variable
		elsif (user_id = cookies.signed[:user_id])
			user = User.find_by(id: user_id)
			#authenticated? method checks if the token matches the remember_digest implemented in the user model.
			if user && user.authenticated?(cookies[:remember_token])
				log_in user_id
				@current_user = user
			end
		end	
	end

	#returns true if the user is logged in, false otherwise. 
	def logged_in?
		!current_user.nil?
	end

	def forget(user)
		user.forget #implemented in the user model. Sets the remember_digest to nil. 
		cookies.delete(:user_id)
		cookies.delete(:remember_token)
	end
	#logs out the current user. 
	def log_out
		forget(current_user) #This is the return variable from the current user method. Not the actual current_user instance variable
		session.delete(:user_id)
		@current_user = nil
	end
	
	#redirects to stored location (or to the default)
	def redirect_back_or(default)
		redirect_to(session[:forwarding_url] || default)
		session.delete(:forwarding_url) #forwarding URL is deleted to prevent subsequent loging attempts from forwarding to the protected page.
	end
	
	# Stores the URL trying to be accessed
	def store_location
		session[:forwarding_url] = request.url if request.get?
	end
end
